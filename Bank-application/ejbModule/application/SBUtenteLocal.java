package application;

import javax.ejb.Local;

import model.Utente;

@Local
public interface SBUtenteLocal {
	public void addUtente(String username,String nome, String cognome,String citta,String password);
	//public Utente getUtente(String username,String password); 
	public void saveRole(String username, String role);

	public Utente getUtente(String username);
}
