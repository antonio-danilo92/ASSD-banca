package model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;

@Entity
@NamedQuery(name="findOperationById", query="Select o from Operazione o WHERE o.idOperazione= :id")
public class Operazione implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
    @SequenceGenerator(name="OP_GEN")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="OP_GEN")
	@Id private int idOperazione;
	private long timestamp;
	private int idC_C;
	private double amount;
	
	/*avevamo previsto che non ci dovesse essere un field con il tipo di operazione
	 * in modo che l'amount ( positivo o negetivo) ci indicasse se l'operazione 
	 * � un deposit o un withdraw rispettivamente. Giusto???
	 * */
	
	public Operazione(){
		super();
	}
	

	
	public int getIdOperazione() {
		return idOperazione;
	}
	public void setIdOperazione(int idOperazione) {
		this.idOperazione = idOperazione;
	}
	public long getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	public int getIdC_C() {
		return idC_C;
	}
	public void setIdC_C(int idC_C) {
		this.idC_C = idC_C;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
}
