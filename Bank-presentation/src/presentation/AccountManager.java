package presentation;


import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.UserTransaction;

import application.SBAccountLocal;
import utility.AccountNotFindException;
import utility.AccountSecurityException;
import utility.WithdrawAmountException;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class AccountManager {
  @EJB private SBAccountLocal account;
  @Resource private SessionContext context;
  @Resource private UserTransaction tx;
public AccountManager(){
	
}

//@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
public ReturnMessage createAccount(HttpServletRequest request){
	ReturnMessage ret;
	String userId;
	try{
		tx.begin();
		userId =context.getCallerPrincipal().getName(); //dinamicamente collega la sessione all'username
		int accountId=account.createAccount(userId);
		if(accountId==0)
			ret= new ReturnMessage(ErrorCodes.INVALID_OP,"Errore creazione nuovo account");
		else
			ret= new ReturnMessage(200,"Nuovo conto corrente "+accountId+"creato");
		tx.commit();
		return ret;
	}catch(Exception e){
		ret= new ReturnMessage(ErrorCodes.INVALID_OP,"Errore creazione nuovo account");
		try{tx.rollback();}catch(Exception e1){
			ret= new ReturnMessage(ErrorCodes.INVALID_OP,"Errore rollback creazione nuovo account");
		}
		return ret;
	}
		
}

//@TransactionAttribute(TransactionAttributeType.REQUIRED)
public ReturnMessage deposit(HttpServletRequest request){
	ReturnMessage ret;
	int accountId= Integer.parseInt(request.getParameter("accountId"));
	double amount= Double.parseDouble(request.getParameter("amount"));
	System.out.println("AccountManager: accountID "+accountId+"\namount "+amount);
	System.out.println(account);
	try {
		tx.begin();
		account.deposit(accountId, amount);
		tx.commit();
		ret= new ReturnMessage(200, "deposito effettuato");
	}  catch(Exception e){
		try{ret= new ReturnMessage(ErrorCodes.SECURITY_ERR, e.getMessage());
		tx.rollback();}
		catch(Exception e1){
			ret= new ReturnMessage(ErrorCodes.SECURITY_ERR, e1.getMessage());
		}		
	}
	return ret;
}

//@TransactionAttribute(TransactionAttributeType.REQUIRED)
public ReturnMessage withdraw(HttpServletRequest request){
	ReturnMessage ret;
	int accountId= Integer.parseInt(request.getParameter("accountId"));
	double amount= Double.parseDouble(request.getParameter("amount"));
	try {
		tx.begin();
		account.withdraw(accountId, amount);
		ret= new ReturnMessage(200, "prelievo effettuato");
		tx.commit();
	} 
	catch(Exception e){
		try{ret= new ReturnMessage(ErrorCodes.SECURITY_ERR, e.getMessage());
		tx.rollback();}
		catch(Exception e1){
			ret= new ReturnMessage(ErrorCodes.SECURITY_ERR, e1.getMessage());
		}
	}
	return ret;
}




}
