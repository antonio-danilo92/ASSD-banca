package interceptors;

import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.SessionContext;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import application.SBUtenteLocal;
import model.Account;
import model.Utente;
import utility.AccountSecurityException;

public class AccountSecurityInterceptor {
	@PersistenceContext EntityManager em;
	@Resource private SessionContext context;
    @EJB private SBUtenteLocal utente;


@AroundInvoke
public Object accountSecurityInterceptor(InvocationContext invocation) throws Exception{
	//getParameters permette di ottenere i valori dei parametri passati al metodo intercettato
	Object[] params=invocation.getParameters();
	int accountId=Integer.parseInt(params[0].toString());
	String username = context.getCallerPrincipal().getName(); //dimanicamente collega la sessione all'username	
	Utente ut=utente.getUtente(username);
	List<Account> accounts=ut.getAccounts();
	//ricerca lineare per semplicità
	for(Account a:accounts){
		if(a.getAccountID()==accountId) return invocation.proceed();
	}
	
	throw new AccountSecurityException("Il tuo account non e' autorizzato ad effettuare operazioni sul conto numero "+accountId);
}
}
