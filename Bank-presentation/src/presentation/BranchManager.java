 package presentation;
 
 import javax.annotation.Resource;
import javax.ejb.EJB;
 import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.UserTransaction;

import application.BranchLocal;
 import utility.AccountNotFindException;
 import utility.AccountSecurityException;
 import utility.WithdrawAmountException;
 
 @Stateless
 @TransactionManagement(TransactionManagementType.CONTAINER)
 public class BranchManager {
 	@EJB BranchLocal branch;
 	@Resource private UserTransaction tx;
 	public BranchManager(){
 		
 	}
 	
 	@TransactionAttribute(TransactionAttributeType.REQUIRED)
 	public ReturnMessage transfer(HttpServletRequest request) {
 		ReturnMessage ret;
 		int accountIdFrom=Integer.valueOf(request.getParameter("accountIdFrom"));
 		int accountIdTo=Integer.valueOf(request.getParameter("accountIdTo"));
 		double amount=Double.valueOf(request.getParameter("amount"));
 		try {
 				branch.transfer(accountIdFrom, accountIdTo, amount);
 				ret= new ReturnMessage(200, "trasferimento effettuato");
 			} catch (AccountNotFindException e) {
 				ret = new ReturnMessage(ErrorCodes.INVALID_OP, e.getMessage());
 			} catch (WithdrawAmountException e) {
 				ret = new ReturnMessage(ErrorCodes.INVALID_OP,e.getMessage());
 			} catch (AccountSecurityException e) {
 				ret = new ReturnMessage(ErrorCodes.SECURITY_ERR,e.getMessage());
 			} 		
 			return ret;
 	}
 }