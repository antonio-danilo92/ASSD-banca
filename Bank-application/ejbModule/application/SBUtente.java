package application;

import javax.annotation.security.PermitAll;
import javax.ejb.LocalBean;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.ejb3.annotation.SecurityDomain;



import model.User;
import model.Utente;

/**
 * Session Bean implementation class SBUtente
 */
@Remote(SBUtenteRemote.class)
@Stateless
@LocalBean
@SecurityDomain("bank")
@TransactionManagement(TransactionManagementType.CONTAINER)
public class SBUtente implements SBUtenteLocal,SBUtenteRemote {

	@PersistenceContext
	EntityManager em;
    /**
     * Default constructor. 
     */
    public SBUtente() {
        // TODO Auto-generated constructor stub
    }

	@Override
	@PermitAll()
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	//REQUIRES_NEW perch� quando la trasnazione termina salva direttamente l'utente nel db, non � pi� necessario fare flush
	public void addUtente(String username,String nome, String cognome, String citta,String password) {
		Utente u=new Utente(username,nome,cognome,citta);
		em.persist(u);
		User c=new User(username,password);
		em.persist(c);
		//em.flush();
		//flush necessario per forzare la scrittura in questo momento sul db. 
		//In caso contrario non � possibile eseguire il metodo saveRole nella stessa chiamata causa uina foreing keysul campo username nella tabella users_roles
		//TODO vedere bene questo fatto da un punto di vista delle transazioni...
	}
	
	@Override
	@PermitAll()
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void saveRole(String username, String role){
		em.createNativeQuery("INSERT INTO users_roles (username, rolename) VALUES ('"+username+"','"+role+"')").executeUpdate();
	}



	@Override
	@PermitAll()
	public Utente getUtente(String username) {
		//System.out.println("getUtente di SBUtente");
		Utente currentUser=em.createNamedQuery("findUtenteByUsername",Utente.class).setParameter("username", username).getSingleResult();
		if(currentUser!=null){
		//System.out.println("nome: "+ currentUser.getNome());
		//System.out.println("nome: "+ currentUser.getCognome());

		
			return currentUser;
		}
		else 
			return null;
	}
}
