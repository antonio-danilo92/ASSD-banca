package presentation;

import java.io.File;
import java.io.IOException;
import java.util.Enumeration;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.SessionContext;
import javax.inject.Inject;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;


import application.SBAccountLocal;
import application.SBUtenteRemote;

/**
 * Servlet implementation class BankController
 */
@WebServlet("/BankController")
public class BankController extends HttpServlet {
	private static final long serialVersionUID = 1L;
    @EJB private UtenteManager um;
    @EJB private AccountManager am;
    @EJB private BranchManager bm;
    @Resource private UserTransaction tx;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public BankController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String op=req.getParameter("op");
		System.out.println("operazione: "+op);
		ReturnMessage respMess = null;
		if(op==null){
			respMess=new ReturnMessage(200,"login.jsp");
			respMess.setMessageAWebPageUrl(true);
		}
		else if(op.equalsIgnoreCase("deposit")){
				respMess=am.deposit(req);			
		}
		else if(op.equalsIgnoreCase("withdraw")){
			respMess=am.withdraw(req);
		}
		else if(op.equalsIgnoreCase("transfer")){
				respMess=bm.transfer(req);
		}
		else if(op.equalsIgnoreCase("logout")){
			req.getSession().invalidate();
			req.logout();
			respMess=new ReturnMessage(200,"home.jsp");
			respMess.setMessageAWebPageUrl(true);
		}
		
		else respMess=new ReturnMessage(ErrorCodes.INVALID_OP,"Operazione non valida");
		sendResponse(req, resp, respMess);
		/*double amount =Double.parseDouble(request.getParameter("amount"));
		try{
			tx.begin();
			int id=branch.createAccount();
			branch.deposit(id,amount);
			tx.commit();
			response.getWriter().println(branch.totalAmount());
		}
		catch(Exception e){
			try {
				tx.rollback();
			} catch (IllegalStateException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (SecurityException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (SystemException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}*/
		
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String op=req.getParameter("op");
		System.out.println("op richiesta: "+op);
		
		ReturnMessage respMess = null;
		if(op.equalsIgnoreCase("addUtente")){
				respMess=um.addUtente(req);
		}
		else if(op.equalsIgnoreCase("createAccount")){
				respMess=am.createAccount(req);
		}
		else respMess=new ReturnMessage(ErrorCodes.INVALID_OP,"Operazione non valida");
		sendResponse(req, resp, respMess);
	}
	/**
	 * Non testato a fondo.
	 * RespMess contiene 3 parametri: codice, che verrà assegnato come codice dell'header http
	 * WebPageUrl: pagina da restituire all'utente (non testato). non è necessario (e non è consigliabile) inserire l'idirizzo ip
	 * Messaggio: messaggio inserito nell'header http, se webpageurl è null, verrà stampato all'utente
	 * 
	 * @param request
	 * @param resp
	 * @param respMess
	 */
	private void sendResponse(HttpServletRequest request,HttpServletResponse resp, ReturnMessage respMess) {
		RequestDispatcher view;
		if(respMess!=null){
			resp.setStatus(respMess.getStatus());
			if(respMess.isMessageAWebPageUrl()){
				view = request.getRequestDispatcher(respMess.getMessage());
				try {
					view.forward(request, resp);
				} catch (ServletException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					resp.setStatus(404);
				}
			}
			else try {
				resp.getWriter().println(respMess.getMessage());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else{
			try {
				resp.getWriter().println("Nessun messaggio di risposta ottenuto");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
			
	}
	

}
