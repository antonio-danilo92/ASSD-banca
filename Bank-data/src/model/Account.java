package model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Account
 *
 */
@Entity
@NamedQuery(name="findAllPositiveAmounts",query="SELECT a from Account a WHERE a.balance>0")
public class Account implements Serializable {
	@SequenceGenerator(name="ACCOUNT_GEN")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ACCOUNT_GEN")
	@Id private int accountID;
	 private double balance;
	
	private static final long serialVersionUID = 1L;

	public Account() {
		super();
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public int getAccountID() {
		return accountID;
	}
   
	public void withdraw(double amount){
		balance-=amount;
	}
	public void deposit(double amount){
		balance+=amount;
	}
}
