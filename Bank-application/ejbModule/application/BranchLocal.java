 package application;
 
 import javax.ejb.Local;
 
 import utility.AccountNotFindException;
 import utility.AccountSecurityException;
 import utility.WithdrawAmountException;
 
 @Local
	
 public interface BranchLocal {
 	public double totalAmount();
 	public void transfer(int accountIdFrom,int accountIdTo,double amount) throws AccountNotFindException, WithdrawAmountException, AccountSecurityException;
 }