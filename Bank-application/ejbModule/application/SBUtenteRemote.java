package application;

import javax.ejb.Remote;

import model.Utente;

@Remote
public interface SBUtenteRemote {
	public void addUtente(String username,String nome, String cognome,String citta,String password);
	public void saveRole(String username, String role);
	public Utente getUtente(String username);

}
