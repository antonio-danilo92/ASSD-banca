package application;

import javax.ejb.Remote;

import utility.AccountNotFindException;
import utility.AccountSecurityException;
import utility.WithdrawAmountException;

@Remote
public interface SBAccountRemote {
	public int createAccount(String idUtente);
	public boolean deposit(int accountId,double amount) throws AccountNotFindException, AccountSecurityException;
	public boolean withdraw(int accountId, double amount) throws AccountNotFindException,WithdrawAmountException, AccountSecurityException;
	}
