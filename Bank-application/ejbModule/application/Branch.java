package application;
 
 import java.util.List;

import javax.annotation.Resource;
import javax.annotation.security.RolesAllowed;
 import javax.ejb.EJB;
 import javax.ejb.LocalBean;
 import javax.ejb.Stateless;
 import javax.ejb.TransactionAttribute;
 import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.interceptor.Interceptors;
 import javax.persistence.EntityManager;
 import javax.persistence.PersistenceContext;
import javax.transaction.NotSupportedException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import org.jboss.ejb3.annotation.SecurityDomain;
 
 import interceptors.AccountSecurityInterceptor;
 import interceptors.DepositInterceptor;
import interceptors.TransferInterceptor;
import utility.AccountNotFindException;
 import utility.AccountSecurityException;
 import utility.WithdrawAmountException;
 import model.Account;
 
 /**
  * Session Bean implementation class Branch
  */
 @Stateless
 @SecurityDomain("bank")
 @TransactionManagement(TransactionManagementType.BEAN)
 public class Branch implements BranchRemote, BranchLocal {
 
 	@EJB SBAccountLocal account;
 	@Resource private UserTransaction tx;
     /**
      * Default constructor. 
      */
 	@PersistenceContext
 	EntityManager em;
     public Branch() {
         // TODO Auto-generated constructor stub
     }
 
 	@Override
 
 	public double totalAmount() {
 		List<Account> l=em.createNamedQuery("findAllPositiveAmounts",Account.class).getResultList();
 		double total=0;
 		for(int i=0;i<l.size();i++){
 				total+=l.get(i).getBalance();
 		}
 		return total;
 	}
 
 	
 	@RolesAllowed("user")
 	@Interceptors({AccountSecurityInterceptor.class, TransferInterceptor.class})
 	public void transfer(int accountIdFrom,int accountIdTo,double amount) throws AccountNotFindException, WithdrawAmountException, AccountSecurityException{
 			try{
 				tx.begin();
 				Account a=em.find(Account.class, accountIdFrom);
 				if(a==null) throw new AccountNotFindException("Account "+accountIdFrom+" non trovato");
 				Account a2=em.find(Account.class, accountIdTo);
 				if(a2==null) throw new AccountNotFindException("Account "+accountIdTo+" non trovato");
 				
 				if(a.getBalance()<amount) throw new WithdrawAmountException("Impossibile prelavare "+ amount+ " il saldo e' "+a.getBalance() );
 				a.withdraw(amount);
 				
 				if(amount<0) throw new AccountNotFindException("Impossibile effettuare depositi di ammontare negativo!!!!!!!!"); 
 				a2.deposit(amount);
	 			tx.commit();
 			}
 			catch(Exception e){
 				try{
					tx.rollback();
					System.out.println("Brench: rollback");
 				}catch(Exception e1){
 					System.out.println("Errore rollback trasferimento");
 				}
					if(e instanceof AccountNotFindException)
						throw new AccountNotFindException(e.getMessage());
					else if(e instanceof WithdrawAmountException)
						throw new WithdrawAmountException(e.getMessage());
					else if (e instanceof AccountSecurityException)
						throw new AccountSecurityException(e.getMessage());
 			} 
 	}
 
 }
