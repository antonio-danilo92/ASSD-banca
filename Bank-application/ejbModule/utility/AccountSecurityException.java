package utility;

public class AccountSecurityException extends Exception {

	public AccountSecurityException() {
		// TODO Auto-generated constructor stub
	}

	public AccountSecurityException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public AccountSecurityException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public AccountSecurityException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public AccountSecurityException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
