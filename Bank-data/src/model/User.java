package model;

import java.io.Serializable;


import javax.persistence.*;

@Entity
@NamedQuery(name="findCredenzialiByUsernameAndPassword",query="SELECT u FROM User u WHERE u.username= :username AND u.password= :password")
@Table(name = "users")
public class User implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	String username;
	String password;
	
	public User(){
		super();
	}

	public User(String username,String password){
		this.username=username;
		this.password=password;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
}
