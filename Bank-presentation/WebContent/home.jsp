<!-- home.jsp -->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
<meta charset="ISO-8859-1">
<title>HOME</title>
</head>
<body>
<p><strong>Utente: 
<a href="
<%
String userId = request.getRemoteUser();
out.println("secured/testInfoUtente.jsp");
%>
">
<%
if(userId!=null) out.println(userId);
else out.println("login");
%></a></strong></p>
<form method="<%
if(userId!=null) out.print("get");
else out.print("post");
%>" action=
<%
if(userId!=null) out.print("/Bank-presentation/BankController");
else out.print("/Bank-presentation/SignUp/testAddUtente.html");
%>

>
<br>
<input type="submit" value="<%
if(userId!=null) out.print("logout");
else out.print("registrati");
%>" id="<%
if(userId!=null) out.print("logout");
else out.print("addUser");
%>
" name="op" />
</form>
<hr />
<p>&nbsp;</p>
</body>
</html>