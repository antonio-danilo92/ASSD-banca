package presentation;

import java.io.IOException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Account;

/**
 * Servlet implementation class InfoAccountServlet
 */
@WebServlet("/InfoAccountServlet")
public class InfoAccountServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	@PersistenceContext EntityManager em;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InfoAccountServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		if(request.getParameter("id_account")!=null){
		int id=Integer.parseInt(request.getParameter("id_account"));
		System.out.println(("id account in infoAccountServlet: "+id));
		
		Account a=em.find(Account.class, id);
		response.getWriter().println("Account Id: "+a.getAccountID());

		response.getWriter().println("Saldo: "+a.getBalance());
		}
		else{
			response.getWriter().println("Nessun conto corrente selezionato");
		}
        
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
