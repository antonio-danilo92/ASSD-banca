package interceptors;

import java.io.Serializable;

import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;
import javax.jms.JMSException;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import model.Operazione;


public class WithdrawInterceptor {
	//@PersistenceContext
	//EntityManager em;
	
@AroundInvoke
public Object withdrawInterceptor(InvocationContext invocation) throws Exception{
	boolean completato=false;
	try{
		Object o =invocation.proceed();
		completato=true;
		return o;
	}finally{
		if (completato){
			System.out.println("COMPLETATO");
	//getParameters permette di ottenere i valori dei parametri passati al metodo intercettato
		Object[] params=invocation.getParameters();
		int accountId=Integer.parseInt(params[0].toString());
		double amount=Double.parseDouble(params[1].toString());
		
	Operazione o=new Operazione();
	//� una operazione di prelievo, quindi amount va memorizzato con segno negativo??
	//oppure aggiungiamo all' entity operazione un campo String tipoOperazione??
	o.setAmount(amount-(2*amount));
	o.setIdC_C(accountId);
	o.setTimestamp(System.currentTimeMillis());
	sendMessage(o);
		}
		else
			System.out.println("EXCEPTION");
	}
	
	//return invocation.proceed();
}
private void sendMessage(Serializable message) throws JMSException, NamingException {
	final String QUEUE_LOOKUP = "java:/queue/storicoQueue";
    final String CONNECTION_FACTORY = "java:/ConnectionFactory";
	Context context = new InitialContext();
    QueueConnectionFactory factory = 
        (QueueConnectionFactory)context.lookup(CONNECTION_FACTORY);
	QueueConnection conn = factory.createQueueConnection();
	QueueSession session = conn.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
	Queue queue = (Queue)context.lookup(QUEUE_LOOKUP);
	conn.start();
	QueueSender sender = session.createSender(queue);
	ObjectMessage mess = session.createObjectMessage();
	mess.setObject(message);
	sender.send(mess);
	System.out.println("MESSAGGIO **PRELIEVO** INVIATO");
	conn.close();
}
}
