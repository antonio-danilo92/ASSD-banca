package model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Utente
 *
 */
@Entity
@NamedQuery(name="findUtenteByUsername", query="SELECT u from Utente u WHERE u.username= :username" ) 
//una possibile query da provare se funziona
public class Utente implements Serializable {
//	@GeneratedValue(strategy=GenerationType.AUTO)
//	@Id private int idUtente;
	private String nome;
	@Id private String username;
	private String cognome;
	private String citta;
	@OneToMany(fetch=FetchType.EAGER) private List<Account> accounts; //da vedere se aggiungere la @OneToMany
	
	private static final long serialVersionUID = 1L;

	// il metodo addAccount(Account a) lo mettiamo nel SessionBean? 
	// idem il metodo removeAccount(Account a)?
	
	public Utente() {
		super();
	}
	
	public Utente(String nome, String cognome){
		super();
		this.nome=nome;
		this.cognome=cognome;
	}
	
	public Utente(String username,String nome, String cognome, String citta){
		this.username=username;
		this.nome=nome;
		this.cognome=cognome;
		this.citta=citta;
		
	}
	/**
	 * @return the idUtente
	 */
//	public int getIdUtente() {
//		return idUtente;
//	}

	/**
	 * @param idUtente the idUtente to set
	 */
//	public void setIdUtente(int idUtente) {
//		this.idUtente = idUtente;
//	}

	/**
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * @param nome the nome to set
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	/**
	 * @return the cognome
	 */
	public String getCognome() {
		return cognome;
	}

	/**
	 * @param cognome the cognome to set
	 */
	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	/**
	 * @return the citta
	 */
	public String getCitta() {
		return citta;
	}

	/**
	 * @param citta the citta to set
	 */
	public void setCitta(String citta) {
		this.citta = citta;
	}

	/**
	 * @return the accounts
	 */
	public List<Account> getAccounts() {
		return accounts;
	}

	/**
	 * @param accounts the accounts to set
	 */
	public void setAccounts(List<Account> accounts) {
		this.accounts = accounts;
	}
	
	

   
}
