 package application;
 
 import javax.ejb.Remote;
 
 import utility.AccountNotFindException;
 import utility.AccountSecurityException;
 import utility.WithdrawAmountException;
 
 @Remote
 public interface BranchRemote {
 	public double totalAmount();
 	public void transfer(int accountIdFrom,int accountIdTo,double amount) throws AccountNotFindException, WithdrawAmountException, AccountSecurityException;
 
 }