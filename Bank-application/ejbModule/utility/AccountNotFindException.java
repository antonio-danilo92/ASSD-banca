package utility;

public class AccountNotFindException extends Exception{
 public AccountNotFindException(String msg){
	 super(msg);
 }
 
}
