package application;

import javax.ejb.Local;

import utility.AccountNotFindException;
import utility.AccountSecurityException;
import utility.WithdrawAmountException;

@Local
public interface SBAccountLocal {

	public int createAccount(String idUtente);
	public boolean deposit(int accountId,double amount) throws AccountNotFindException, AccountSecurityException;
	public boolean withdraw(int accountId, double amount) throws AccountNotFindException,WithdrawAmountException, AccountSecurityException;
	}
