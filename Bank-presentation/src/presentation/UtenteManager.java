package presentation;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.UserTransaction;

import model.Utente;
import application.SBAccountLocal;
import application.SBUtente;
import application.SBUtenteLocal;
import application.SBUtenteRemote;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class UtenteManager {
    @EJB private SBUtenteLocal utente;
    @Resource SessionContext context;
    @Resource private UserTransaction tx;

	public UtenteManager(){}
	
	public  ReturnMessage addUtente(HttpServletRequest request){
		ReturnMessage ret;
		String username=request.getParameter("username");	
		String password=request.getParameter("password");	
		String nome=request.getParameter("nome");
		
		String cognome=request.getParameter("cognome");
		String citta=request.getParameter("citta");
		try{
			tx.begin();
			utente.addUtente(username, nome, cognome, citta, password);
			//se fallisce saveRole ho cmq inserito un utente nel db senza il relativo ruolo... dovrei toglierlo a mano
			utente.saveRole(username, "user");
			ret= new ReturnMessage(200,"login.jsp");
			ret.setMessageAWebPageUrl(true);
			tx.commit();
		}
		catch(Exception e){
			ret= new ReturnMessage(ErrorCodes.INVALID_OP, "Impossibile inserire l'utente "+username);
			try{tx.rollback();}
			catch(Exception e1){ret= new ReturnMessage(ErrorCodes.INVALID_OP, e1.getMessage());}
		}
		return ret;
		}
	
	
	public Utente getUtente(HttpServletRequest request){
		System.out.println("getUtente di UtenteManager");
//		String username=context.getCallerPrincipal().getName();
//		System.out.println("username from context: "+ username);
	String username="a";
		Utente u=	utente.getUtente(username);
		return u;
	}

}
