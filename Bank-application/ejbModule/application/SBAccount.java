package application;

import interceptors.AccountSecurityInterceptor;
import interceptors.DepositInterceptor;
import interceptors.WithdrawInterceptor;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.annotation.security.RolesAllowed;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import org.jboss.ejb3.annotation.SecurityDomain;

import model.Account;
import model.Utente;
import utility.AccountNotFindException;
import utility.AccountSecurityException;
import utility.WithdrawAmountException;

/**
 * Session Bean implementation class SBAccount
 */
@Remote(SBAccountRemote.class)
@Stateless
@SecurityDomain("bank")
@TransactionManagement(TransactionManagementType.CONTAINER)
public class SBAccount implements SBAccountLocal, SBAccountRemote{

	@PersistenceContext
	EntityManager em;
	@Resource private UserTransaction tx;
    /**
     * Default constructor. 
     */
    public SBAccount() {
        
    }

	@Override
	@RolesAllowed("user")
	@TransactionAttribute(TransactionAttributeType.REQUIRED) 
	//REQUIRED perch� il metodo viene chiamato in AccountManager che prevede gi� la gestione di una transazione
	// mettere REQUIRES_NEW significa che se createAccount va a buon fine ma fallisce la transazione amonte resta comunque salvato il nuovo account nel db
	public int createAccount(String idUtente) {
		//gestire le transazioni: creo l'accunt, ma poi lo devo collegare all'utente, deve andare tutto a buon fine.
			Utente u= em.find(Utente.class, idUtente);
			if(u==null) return 0;
			Account a=new Account();
			em.persist(a);	
			addAccount(u,a);
			return a.getAccountID();
	}
	
	@Override
	@Interceptors({AccountSecurityInterceptor.class,DepositInterceptor.class})
	@RolesAllowed("user")
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public boolean deposit(int accountId, double amount) throws AccountNotFindException, AccountSecurityException {
		Account a=em.find(Account.class, accountId);
		if(a==null) throw new AccountNotFindException("Account "+accountId+" non trovato");
		if(amount<0) throw new AccountNotFindException("Impossibile effettuare depositi di ammontare negativo!!!!!!!!"); 
		/// dovrei lanciare un'eccezione diversa, no AccountNotFindException, ma poi devo cambiare la signature del metodo.....
		a.deposit(amount);
		return true;
	}

	//con la gestione delle eccezioni non � pi� tanto utile far ritornare un boolean...
	@Override
	@Interceptors({AccountSecurityInterceptor.class,WithdrawInterceptor.class})
	@RolesAllowed("user")
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public boolean withdraw(int accountId, double amount) throws AccountNotFindException,WithdrawAmountException, AccountSecurityException  {
		Account a=em.find(Account.class, accountId);
		if(a==null) throw new AccountNotFindException("Account "+accountId+" non trovato");
		if(a.getBalance()<amount) throw new WithdrawAmountException("Impossibile prelavare "+ amount+ " il saldo e' "+a.getBalance() );
		a.withdraw(amount);
		return true;
	}
	
	
	//perch� aggiungiamo l'account alla lista degli account dell'utente? quando si salva poi questa cosa??
	//TODO mettere la transazione anche qui???
	private void addAccount(Utente u, Account a){
		List<Account> accounts= u.getAccounts();
		if(accounts==null) accounts= new ArrayList<Account>();
		accounts.add(a);
	}

}
