<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Informazioni Utente</title>
</head>
<body>
 
<form method="get" action="/Bank-presentation/BankController">
<%@ page import = "application.SBUtenteRemote"%>

<%@ page import = "javax.ejb.EJB"%>
<%@ page import = "javax.annotation.Resource"%>
<%@ page import="javax.naming.InitialContext" %>
<%@ page import="javax.naming.Context" %>
<%@ page import="java.util.Properties" %>
<%@ page import="model.Utente" %>
<%@ page import="java.util.List" %>
<%@ page import="model.Account" %>
<%@ page import="javax.persistence.EntityManager" %>
<%@ page import="javax.persistence.PersistenceContext" %>


 
<%!
@PersistenceContext EntityManager em;
SBUtenteRemote sbUtente;
Utente u;
String username,nome,cognome,citt�;
Context initCtx,envCtx;
Properties prop;
List<Account> accountsList;
%>

<!-- codice relativo al recupero delle informazioni personali;
 -->
<%username=request.getRemoteUser();

prop = new Properties();
prop.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");

 initCtx = new InitialContext(prop);
 
 sbUtente = (SBUtenteRemote) initCtx.lookup("ejb:Bank-EAR/Bank-application/SBUtente!application.SBUtenteRemote");
 u=sbUtente.getUtente(username);
 nome=u.getNome();
 cognome=u.getCognome();
 citt�=u.getCitta();
 
%>
<input type="submit" value="Home" id="Home" formaction="/Bank-presentation/home.jsp">
<br>
<br>

<fieldset>
<legend><strong><big>Dati Personali</big></strong></legend>
<label><strong><big>Nome:</big> </strong><%=nome%></label><br>
<label><strong><big>Cognome: </big></strong><%=cognome%> </label><br>
<label><strong><big>Citt�: </big></strong> <%=citt�%></label><br>
</fieldset>
<br>


<fieldset>
<legend><strong>Scelta operazioni</strong></legend>
<input type="submit" value="Deposit/withdraw" id="deposit/withdraw" formaction="testDepositWithdraw.html">
<input type="submit" value="Transfer" id="tranfer" formaction="testTransfer.html">
<input type="submit" value="Open new account" id="New account" formaction="testAddAccount.html">

</fieldset><br>

<!-- codice relativo al recupero delle info dei conti correnti personali 
Viene recuperato l'entity manager. Per permettere tale operazione � necessario settare nel file persistence.xml
la propriet� 		<property name="jboss.entity.manager.jndi.name" value="java:/Manager1"/>
in modo da associare all'entity manager un nome jndi
-->
<%
accountsList=u.getAccounts();
prop = new Properties();
prop.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");

 initCtx = new InitialContext(prop);
 em=(EntityManager)initCtx.lookup("java:/Manager1");
%>

 
<fieldset>
<legend><strong>Informazioni Conti Personali</strong></legend>
<label>Seleziona conto corrente</label>
<select id=id_account name=id_account >
<%for (int i=0; i<accountsList.size();i++){ %>
<option value =<%=accountsList.get(i).getAccountID() %> >   <%=accountsList.get(i).getAccountID() %> 
</option>
<%} %>
</select>

<input type="submit" value="Info conto" id="infoCC" formaction="/Bank-presentation/InfoAccountServlet">
</fieldset>
</form>
</body>
</html>