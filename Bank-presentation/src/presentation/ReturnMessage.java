package presentation;

public class ReturnMessage {
	private int statusCode;
	private String respMessage;
	private boolean isMessageAWebPageUrl=false;
	public ReturnMessage(int statusCode, String respMessage) {
		this.statusCode=statusCode;
		this.respMessage=respMessage;
	}
	
	public int getStatus() {
		return statusCode;
	}
	public void setStatus(int statusCode) {
		this.statusCode = statusCode;
	}
	public String getMessage() {
		return respMessage;
	}
	public void setMessage(String respMessage) {
		this.respMessage = respMessage;
	}

	public boolean isMessageAWebPageUrl() {
		// TODO Auto-generated method stub
		return isMessageAWebPageUrl;
	}

	public void setMessageAWebPageUrl(boolean isMessageAWebPageUrl) {
		this.isMessageAWebPageUrl = isMessageAWebPageUrl;
	}

}
